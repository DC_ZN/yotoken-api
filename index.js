const CONTRACT_ADDRESS = '0x5Bdf2d300D787f68bA21Fd12fcDA595762027E2E';
const MANAGER_ADDR = '0xe278E27e4a9E2Dc6D9a52Dfcb10e1c538c2087cD';
const APP_PORT = 3000;

var express = require('express');
var multer = require('multer');
var app = express();

var ethWallet = require('ethereumjs-wallet');
var Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

var tokenAbi = require('./contract.json');
var yothereumContract = new web3.eth.Contract(tokenAbi, CONTRACT_ADDRESS);

var upload = multer();

app.post('/loan', upload.single('wallet'), function (req, res) {
	var rawWallet = JSON.parse(req.file.buffer.toString());
	if (rawWallet.Crypto) {
		rawWallet.crypto = rawWallet.Crypto;
	}

	var wallet = ethWallet.fromV3(rawWallet, req.body.password);
	var amount = (parseInt(req.body.amount, 10) * 100).toString();

	web3.eth.sendTransaction({ from: MANAGER_ADDR, to: wallet.getAddressString(), value: web3.utils.toWei('10') });

	var resSent = false;

	yothereumContract.methods.getLoan(wallet.getAddressString(), amount).send({ from: MANAGER_ADDR })
		.on('transactionHash', function () {
			if (!resSent) {
				res.send({ ok: true, address: wallet.getAddressString() });
				resSent = true;
			}
		})
		.on('error', function (e) {
			console.error(e);
			if (!resSent) {
				res.send({ error: e.toString() });
				resSent = true;
			}
		});
});

app.get('/balance/:address', function (req, res) {
	const address = req.params.address.toString();

	if (!address.startsWith('0x')) {
		res.send({ error: 'invalid address' });
		return;
	}

	yothereumContract.methods.balanceOf(address).call()
		.then(function (balance) {
			res.send({ ok: true, balance: balance });
		});
});

app.get('/owed/:address', function (req, res) {
	const address = req.params.address;

	if (!address.startsWith('0x')) {
		res.send({ error: 'invalid address' });
		return;
	}

	yothereumContract.methods.owed(address).call().then(function (balance) {
		res.send({ ok: true, balance: balance });
	});
});

app.use(function (err, req, res, next) {
	console.error(err.stack);
	res.status(500).send({ error: err.toString() });
});

app.listen(APP_PORT, function () {
	console.log('Listening at port ' + APP_PORT);
});

